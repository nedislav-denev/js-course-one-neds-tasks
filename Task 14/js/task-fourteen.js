task14 = function() {
    'use strict';
    /*
    Given a string, if its length is at least 3, add 'ing' to its end. Unless it already ends in 'ing', in
    which case add 'ly' instead. If the string length is less than 3, leave it unchanged. Return the
    new string. For example:
    verbing('verb')
    result verbing
    verbing('swimming')
    result swimmingly
    verbing('do')
    result do
    */

    /***
    * Function that takes a string and if its length is 3 characters or more adds 'ing' at the end unless it already ends in 'ing' then it adds 'ly'
    * param {String} str
    * returns the original string if its less than 3 chars long or the original string + 'ing' unless it already ends in 'ing' then it adds 'ly' and returns the new string, {String}
    ***/
    var verbing = function( str ) {
        var newStr = '',
            tempStr = '',
            strLen = str.length;
        // Check if both the entry parameters are strings
        if ( typeof str === 'string') {
            // Check how long the entry parameter is
            if ( strLen >= 3 ) {
                // Save the last 3 characters to a temporary variable
                tempStr = str.substr(strLen - 3);
                // If the last 3 characters are 'ing' then add 'ly' to the new string
                if ( tempStr === 'ing' ) {
                    newStr = str + 'ly';
                }
                // otherwise add 'ing'
                else {
                    newStr = str + 'ing';
                }
            }
            // Return the original string if its less than 3 chars long
            else {
                newStr = str;
            }
        } else {
            throw new TypeError('The entered parameter is not a string!');
        }
        return newStr;
    };
    console.log(['loving', verbing('loving')]);
    console.log(['hi', verbing('hi')]);
    console.log(['lol', verbing('lol')]);
};
task14();