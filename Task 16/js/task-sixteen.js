task16 = function() {
    'use strict';
    /*
    Write a function that removes an item from a random location in the given array and returns
    the removed item. (Be careful to return just the item, not an array of length 1 that contains the
    item.) If the array is empty, do nothing and return null. Some examples (but note that the
    results are supposed to be random and should vary between runs):
    ["foo", "bar", "baz"]: "bar" leaving ["foo", "baz"]
    []: null leaving []
    removeRandomItem(["foo", "bar", "baz"])
    result "bar" leaving ["foo", "baz"]
    removeRandomItem([42])
    result 42 leaving []
    removeRandomItem([])
    result null leaving []
    */

    /***
    * Function that takes an array and removes a random item from it.
    * param {Array} entryArray
    * returns the removed item + the left over elements of the original array, {String}
    ***/
    var removeRandomItem = function( entryArray ) {
    	// store the random item here
    	var randomItem = '',
    	// store the removed array type item here
    		removed = '',
		// the return variable
    		result;
		// check if we have an array as entry parameter
    	if ( entryArray.constructor === Array ) {
    		// check if the array has any items
    		if ( entryArray.length > 0 ) {
    			// randomly pick an item from the array
    			randomItem = entryArray[Math.floor(Math.random()*entryArray.length)];
    			// save the removed random item in a variable
    			removed = entryArray.splice(entryArray.indexOf(randomItem), 1);
    			result = randomItem;
    		} else {
    			// return null if we don't have any items in the array
    			result = null;
    		}
    	} else {
    	    throw new TypeError('Entry parameter is not an array');
    	}
    	return 'result ' + result + ' leaving ' + entryArray;
    };

    console.log(removeRandomItem([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]));
    console.log(removeRandomItem([]));
};
task16();