task11 = function() {
    'use strict';
    // Given an amount, return '<amount> leva', except add '($$$)' at the end if the amount is 1
    // million. For example:
    // chekValue(10)
    // result 10 leva
    // chekValue(42)
    // result 42 leva
    // chekValue(1000000)
    // result 1000000 dollars ($$$)

    /***
    * Function that takes a number and returns the <amount> + 'leva'
    * except for when the number is 1 million -> returns <amount> + '($$$)'
    * param {Number} dolla
    * returns the amount entered + 'leva' or <amount> + '($$$)' depending on the number entered, {String}
    ***/
    var moneyInTheBank = function( dolla ) {
        var daMoney = '';
        if ( typeof dolla === 'number' ) {
            if ( dolla == 1000000) {
                daMoney = dolla + ' dollars ($$$)!';
            } else if ( dolla == 1 ) {
                daMoney = dolla + ' lev';
            } else {
                daMoney = dolla + ' leva';
            }
        } else {
            throw new TypeError('Entry is not a number!');
        }

        return daMoney;
    };

    console.log(moneyInTheBank(1337));
};
task11();