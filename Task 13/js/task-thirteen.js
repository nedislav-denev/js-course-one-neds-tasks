task13 = function() {
    'use strict';
    // Given a string, return a version where all occurrences of its first character have been replaced
    // with '*', except do not change the first character itself. You can assume that the string is at
    // least one character long. For example:
    // fixStart('babble')
    // result ba*ble
    // fixStart('aardvark')
    // result a*rdvark
    // fixStart('google')
    // result goo*le

    /***
    * Function that takes a string and replaces all the occurences of its first character with a '*'
    * except for the first character itself.
    * param {String} str
    * returns a string with all the replaced characters, {String}
    ***/
    var charSwapper = function( str ) {
        // Save the first character to a variable
        var firstLetter = str[0],
        // Temporary string
            temp = '',
        // The newly formed string
            newStr = '',
        // Regular expression for the first char
            regEx = new RegExp(firstLetter, 'g');

        // Change all occurences of the first character
        temp = str.replace(regEx, '*');
        // Place the original character back
        newStr = temp.replace('*', firstLetter);

        return newStr;
    };
    console.log(charSwapper('чичовитечервенотиквеничета'));
};
task13();