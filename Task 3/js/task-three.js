window.onload = function() {
    'use strict';
    var vowelChecker = function( randomString ) {
        var vowel,
            checker;

        if ( typeof randomString === 'string' && randomString.length === 1 ) {

            checker = randomString.match(/[aeiouAEIOU]/);

            if ( checker !== null ) {
                vowel = true;
            } else {
                vowel = false;
            }

        } else {
            vowel = 'Enter a single character string!';
        }

        return vowel;
    };

    console.log(['first version', vowelChecker('E')]);

    var fasterVowelCheck = function( char ) {

        if ( typeof char === 'string' && char.length === 1 ) {
            return /[aeiouAEIOU]/.test(char);
        }
    };

    console.log(['second version', fasterVowelCheck('Q')]);

    var isVowel = function(str) {
        var validVowel = ['a', 'e', 'o', 'u', 'i', 'A', 'E', 'O', 'U', 'I'];

        if ( typeof str === 'string' && str.length === 1 ) {

            return validVowel.indexOf(str) != -1;

        } else {
            throw new TypeError('Not a vowel');
        }
    };

    console.log(['third version', isVowel('A')]);
};