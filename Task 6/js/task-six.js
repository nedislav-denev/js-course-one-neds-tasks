var task6 = function() {
    'use strict';
    // Define a function reverse() that computes the reversal of a string. For example, reverse("jag
    // testar") should return the string "ratset gaj".

    /***
    * Function that reverts the string it accepts
    * param {String} arg
    * returns the reversed string, {String}
    ***/
    var reverse = function( arg ) {
        var reverseString = '';
        // Check if entry parameter is a string
        if ( typeof arg === 'string') {
            reverseString = arg.split('').reverse().join('');
        } else {
            throw new TypeError('Entry is not a string!');
        }
        return reverseString;
    };

    console.log(reverse('Аз обичам Йоана'));
};
task6();