var task5 = function() {
    'use strict';
    // Task 5 - homework
    // Define a function sum() and a function multiply() that sums and multiplies (respectively) all the
    // numbers in an array of numbers. For example, sum([1,2,3,4]) should return 10, and
    // multiply([1,2,3,4]) should return 24.

    /***
    * Function that sums all the numbers in an array
    * param {Array of Numbers} params
    * returns the sum, {Number}
    ***/
    var sum = function( params ) {
        var summedValue = 0;
        // Check if entry parameter is an array
        if ( params.constructor === Array ) {
            for ( var i = 0; i < params.length; i++ ) {
                // Check if current element of the array is a number
                if ( typeof params[i] === 'number') {
                    summedValue += params[i];
                } else {
                    throw new TypeError('Entry parameters is not a number');
                }
            }
        } else {
            throw new TypeError('Entry parameter is not an array');
        }
        return summedValue;
    };
    console.log(sum([2, 3, 4, 5, 6, 15, 20, 222]));
    /***
    * Function that multiplies all the numbers in an array
    * param {Array of Numbers} params
    * returns the multiplication, {Number}
    ***/
    var multiply = function( params ) {
        var multipliedValue = 1;
        // Check if entry parameter is an array
        if ( params.constructor === Array ) {
            for ( var i = 0; i < params.length; i++ ) {
                // Check if current element of the array is a number
                if ( typeof params[i] === 'number') {
                    multipliedValue *= params[i];
                } else {
                    throw new TypeError('Entry parameters is not a number');
                }
            }
        } else {
            throw new TypeError('Entry parameter is not an array');
        }
        return multipliedValue;
    };
    console.log(multiply([2, 3, 4, 5, 6, 15, 20, 222]));
};
task5();