task12 = function() {
    'use strict';
    // Given two strings, return the concatenation of the two strings (separated by a space) slicing
    // out and swapping the first 2 characters of each. You can assume that the strings are at least
    // 2 characters long. For example:
    // mixUp('mix', 'pod')
    // result pox mid
    // mixUp('dog', 'dinner')
    // result dig donner
    // mixUp('gnash', 'sport')
    // result spash gnort

    /***
    * Function that takes two strings and swaps their first 2 characters.
    * param {String} string1
    * param {String} string2
    * returns a concatenated string of the 2 with the first 2 characters swapped and the string separated by a space, {String}
    ***/
    var stringSwapper = function( string1, string2 ) {
        // define variables
        /* Not sure if I should have so many variables for such a simple task */
        var tempStrOne = '',
            tempStrTwo = '',
            newStringOne = '',
            newStringTwo = '';
        // Check if both the entry parameters are strings
        if ( typeof string1 === 'string' && typeof string2 === 'string') {
            // Slice out the first 2 characters
            tempStrOne = string1.slice(0, 2);
            tempStrTwo = string2.slice(0, 2);
            // Swap the first 2 characters in new string variables
            newStringOne = string1.replace(tempStrOne, tempStrTwo);
            newStringTwo = string2.replace(tempStrTwo, tempStrOne);

            return  newStringOne.concat(' ', newStringTwo);

        } else {
            throw new TypeError('The entered parameters are not strings!');
        }

    };
    console.log(stringSwapper('dog', 'dinner'));
};
task12();