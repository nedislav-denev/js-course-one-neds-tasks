window.onload = function() {
    'use strict';
    var max = function( num1, num2, num3 ) {
        var max;
        if ( typeof num1 === 'number' && typeof num2 === 'number' && typeof num3 === 'number' ) {

            if ( num1 > num2 && num1 > num3 ) {
                max = num1;
            } else if ( num2 > num1 && num2 > num3 ){
                max = num2;
            } else {
                max = num3;
            }

        } else {
        max = 'ENTER NUMBERS!';
        }

        return max;
    };

console.log(max(42, 69, 80));
};