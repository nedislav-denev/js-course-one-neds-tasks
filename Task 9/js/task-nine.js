task9 = function() {
    'use strict';
    // Write a function filterLongWords() that takes an array of words and an integer i and returns
    // the array of words that are longer than i.

    /***
    * Function that takes an array and returns an array of words that are longer than the second parameter
    * param {Array of Strings} arrayOfWords
    * param {Number} threshold
    * returns an array of words that are longer than the threshold, {Array}
    ***/
    var filterLongWords = function( arrayOfWords, threshold ) {
        var newWordArray = [];
        // Validate that we have an array as a parameter
        if ( arrayOfWords.constructor === Array && typeof threshold === 'number' ) {
            for ( var i = 0; i < arrayOfWords.length; i++ ) {
                // Make sure that we're only dealing with strings and the threshold is a number
                /* Самуиле, дай предложение къде да сложа проверката */
                // if ( typeof arrayOfWords[i] === 'string') {
                    if ( arrayOfWords[i].length > threshold ) {
                        newWordArray.push(arrayOfWords[i]);
                    }
                // } else {
                    // throw new TypeError('Wrong entry format "( [Array], Number )"');
                // }
            }
        } else {
            throw new TypeError('Entry parameter is not an array');
        }
        return newWordArray;
    };

    console.log(filterLongWords( ['пармезан', 'люта чушка', 'кьопулу', 'гладен съм'], 8 ));
};
task9();