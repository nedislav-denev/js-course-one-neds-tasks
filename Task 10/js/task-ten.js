task10 = function() {
    'use strict';
    // Write a function charFreq() that takes a string and builds a frequency listing of the characters
    // contained in it. Represent the frequency listing as a Javascript object. Try it with something
    // like charFreq("abbabcbdbabdbdbabababcbcbab").

    /***
    * Function that takes a string and returns an object containing the frequency of the listed characters in the string
    * param {String} entryString
    * returns an object containing the frequency of the listed characters in the string, {Object}
    ***/
    var charFreq = function ( entryString ) {
        var frequencyCounter = {};
        // Validate parameter is string
        if ( typeof entryString === 'string') {
            // Cycle through the string
            for ( var i = 0; i < entryString.length; i++ ) {
                // If it exists in the frequencyCounter just increment
                if( frequencyCounter[entryString.charAt(i)] ) {
                    frequencyCounter[entryString.charAt(i)]++;
                }
                // otherwise set value to '1'
                else {
                    frequencyCounter[entryString.charAt(i)] = 1;
                }
            }
        } else {
            throw new TypeError('Entry is not a string!');
        }

        return frequencyCounter;
    };

    console.log(charFreq('Бат Георги, задушавам саааааааа! ОТВОРИ!'));
};
task10();