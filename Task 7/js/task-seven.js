var task7 = function() {
    'use strict';
    // Represent a small bilingual lexicon as a Javascript object in the following fashion
    // {"merry":"god", "christmas":"jul", "and":"och", "happy":gott", "new":"nytt", "year":"år"} and use it
    // to translate your Christmas cards from English into Swedish.

    // Object that contains the translation of English to Swedish
    var dictionary = {
        'merry': 'God',
        'christmas': 'Jul',
        'and': 'och',
        'happy': 'Gott',
        'new': 'Nytt',
        'year': 'år'
    };

    // Forming the string of the xMas card
    var xmasCard =
        dictionary.merry + ' ' +
        dictionary.christmas + ' ' +
        dictionary.and + ' ' +
        dictionary.happy + ' ' +
        dictionary.new + ' ' +
        dictionary.year;

     /***
    * Function that combines all the elements of the dictionary object
    * param {None}
    * returns the translated xmas card, {String}
    ***/
    var cardGenerator = function() {
        var lazyXmasCard = 'The lazy version of the card is: ';
        for (var key in xmasCard) {
          if (xmasCard.hasOwnProperty(key)) {
            lazyXmasCard += xmasCard[key];
          }
        }
        return lazyXmasCard;
    };
    console.log('Merry Christmas and Happy New Year in Swenglish would be: ' + xmasCard);
    console.log(cardGenerator());
};
task7();