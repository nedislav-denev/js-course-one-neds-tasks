(function(win) {

    /***
     * Singleton Instance
     * Draws data in to table
     */
    win.mySingleton = (function() {

        // Instance stores a reference to the Singleton
        var instance;

        function init() {

            /***
             * Private methods and variables
             * like data
             */

             // Add this template when you're not falling asleep
             // var tableTemplate = [
             //    '<table>',
             //        '<tr>',
             //            '<td>',
             //                '<th>',
             //                '</th>',
             //            '</td>',
             //        '</tr>',
             //    '</table>'];

            /***
             * @param {Object} data
             * returns {String} HTML to be injected in the table
             */
            function drawTable() {
                var tableHTML =
                '<table>'+
                    '<tr>';
                // Target the first object in the JSON
                var firstElement = data[0];
                // Cycle through its elements
                for (var key in firstElement){
                    if (firstElement.hasOwnProperty(key)) {
                        // Generate the table header row
                         tableHTML +=
                            '<th style="border: 2px solid black; color: red;">' + key + '</th>';
                    }
                }
                tableHTML += '</tr>';

                // Cycle through the parsed JSON object
                $.each(data, function(key, valueOf) {
                    tableHTML += '<tr>';
                    // Cycle through the inner object
                    $.each(valueOf, function(innerKey, innerValue) {
                        tableHTML +=
                            '<td style="border: 1px solid '+ getRandomColor() + ';">' +
                                innerValue +
                            '</td>';
                    });
                    tableHTML += '</tr>';
                });

                tableHTML += '</table>';

                return tableHTML;
            }

            // Random color generator, because why not?
            function getRandomColor() {
                var letters = '0123456789ABCDEF'.split('');
                var color = '#';
                for (var i = 0; i < 6; i++ ) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                return color;
            }

            // Wannabe INIT function
            function start (opts, el) {
                var element = document.querySelector(el);
                console.log(element)
                element.innerHTML = drawTable();
            }

            // Fire this Frankestein's monster UP
            var PublicAPI = {
                start: start
            };

            // Public methods and variables
            return PublicAPI;
        }

        // Create a new Singleton instance
        return {
            getInstance: function(opts) {
                if(!instance) {
                    instance = init(opts);
                }

                return instance;
            }
        };

    })();

    //Document Ready
    $(function() {
        // Usage:
        var app = mySingleton.getInstance();
        var app2 = mySingleton.getInstance();

        app.start(data, '#table-wrapper');
        app2.start(data, '#table-second-wrapper');

        console.log(app == app2);
    });

})(window);

// THE FAT JSON, re-do this with an AJAX request
var data = [{
        "ID" : 1,
        "First Name" : "Samuil",
        "Last Name" : "Gospodinov",
        "Gender" : "male"
    }, {
        "ID" : 2,
        "First Name" : "Vasil",
        "Last Name" : "Marchev",
        "Gender" : "male"
    }, {
        "ID" : 3,
        "First Name" : "Stanislav",
        "Last Name" : "Kumanov",
        "Gender" : "male"
    }, {
        "ID" : 4,
        "First Name" : "Dimitur",
        "Last Name" : "Dreharov",
        "Gender" : "male"
    }, {
        "ID" : 5,
        "First Name" : "Anna",
        "Last Name" : "Jelqzkova",
        "Gender" : "female"
    }, {
        "ID" : 6,
        "First Name" : "Georgi",
        "Last Name" : "Mihailov",
        "Gender" : "male"
    }, {
        "ID" : 7,
        "First Name" : "Elina",
        "Last Name" : "Bakardjieva",
        "Gender" : "female"
    }, {
        "ID" : 8,
        "First Name" : "Ivan",
        "Last Name" : "Todorov",
        "Gender" : "male"
    }, {
        "ID" : 9,
        "First Name" : "Pavel",
        "Last Name" : "Gaytanski",
        "Gender" : "male"
    }, {
        "ID" : 10,
        "First Name" : "Viktor",
        "Last Name" : "Hristov",
        "Gender" : "male"
    }, {
        "ID" : 11,
        "First Name" : "Nedislav",
        "Last Name" : "Denev",
        "Gender" : "male"
    }, {
        "ID" : 12,
        "First Name" : "Kaloyan",
        "Last Name" : "Ivanov",
        "Gender" : "male"
    }, {
        "ID" : 13,
        "First Name" : "Bat",
        "Last Name" : "Sali",
        "Gender" : "male"
    }, {
        "ID" : 14,
        "First Name" : "Todor",
        "Last Name" : "Gruev",
        "Gender" : "male"
    }, {
        "ID" : 15,
        "First Name" : "Kyle",
        "Last Name" : "Simpson",
        "Gender" : "male"
    }, {
        "ID" : 16,
        "First Name" : "Dimitrina",
        "Last Name" : "Simova",
        "Gender" : "female"
    }, {
        "ID" : 17,
        "First Name" : "Azis",
        "Last Name" : "Vasko-Mangala",
        "Gender" : "shemale"
    }, {
        "ID" : 18,
        "First Name" : "Uzomki",
        "Last Name" : "Naruto",
        "Gender" : "male"
    }, {
        "ID" : 19,
        "First Name" : "Ivan",
        "Last Name" : "Ganev",
        "Gender" : "male"
}];