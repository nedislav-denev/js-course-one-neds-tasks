task15 = function() {
    'use strict';
    /*
    Given a string, find the first appearance of the substring 'not' and 'bad'. If the 'bad' follows the
    'not', replace the whole 'not'...'bad' substring with 'good' and return the result. If you don't find
    'not' and 'bad' in the right sequence (or at all), just return the original sentence. For example:
    'This dinner is not that bad!': 'This dinner is good!'
    'This dinner is bad!': 'This dinner is bad!'
    notBad('This dinner is not that bad!')
    result This dinner is good!
    notBad('This movie is not so bad')
    result This movie is good
    notBad('This tea is not hot')
    result This tea is not hot
    notBad('It's bad yet not')
    result It's bad yet not
    */

    /***
    * Function that takes a string and if there is "not" substring before a "bad" one - then replaces the section with "good". It otherwise returns the old string.
    * param {String} str
    * returns a new version of the string that replaces all the substring between "not" and "bad" if there is such a combination, {String}
    ***/
    var notBad = function ( str ) {
    	// locate 'not' in the string
    	var notPos = str.search('not'),
    	// locate 'bad' in the string
    		badPos = str.search('bad'),
    		newStr = '';
		// Check if we have a string as an entry parameter
    	if ( typeof str === 'string') {
    		// Check if 'not' is before 'bad' and if they are present in the string alltogether
    		if ( notPos < badPos && notPos != -1 && badPos != -1 ) {
    			// Get the content of the string before 'not' add 'good' and get the content of the string after 'bad'
    			newStr = str.substring( 0, notPos ) + 'good' + str.substring( badPos + 3 );
    		} else {
    			// assign the old string to the returned variable
    			newStr = str;
    		}
    	} else {
    		 throw new TypeError('The entered parameter is not a string!');
    	}
    	return newStr;
    };

    console.log(['This movie is so bad!', notBad('This movie is so bad!')]);
    console.log(['This task was not that bad :)', notBad('This task was not that bad :)')]);
};
task15();