task17 = function() {
    'use strict';
    /*
    Write a function that reorders the items in an array into a random order. The function does not
    return anything, but rather moves items around the given array. There are many ways to solve
    this exercise, but one of the simplest is to repeatedly call removeRandomItem() from the
    previous question to obtain all the items in random order one by one. Some examples (but
    note that the results are supposed to be random and should vary between runs):
    ["foo", "bar", "baz"]: ["bar", "foo", "baz"]
    ["a", "b", "c", "d", "e", "f"]: ["d", "f", "e", "c", "b", "a"]
    []: []
    randomizeOrder(["foo", "bar", "baz"])
    result ["bar", "foo", "baz"]
    randomizeOrder(["a", "b", "c", "d", "e", "f"])
    result ["e", "d", "f", "b", "c", "a"]
    randomizeOrder([])
    return []
    */

    /***
    * Function that takes an array and removes a random item from it.
    * param {Array} entryArray
    * returns the removed item + the left over elements of the original array, {String}
    ***/
    var removeRandomItem = function( entryArray ) {
    	// store the random item here
    	var randomItem = '',
    	// store the removed array type item here
    		removed = '',
		// the return variable
    		result;
		// check if we have an array as entry parameter
    	if ( entryArray.constructor === Array ) {
    		// check if the array has any items
    		if ( entryArray.length > 0 ) {
    			// randomly pick an item from the array
    			randomItem = entryArray[Math.floor(Math.random()*entryArray.length)];
    			// save the removed random item in a variable
    			removed = entryArray.splice(entryArray.indexOf(randomItem), 1);
    			result = randomItem;
    		} else {
    			// return null if we don't have any items in the array
    			result = null;
    		}
    	} else {
    	    throw new TypeError('Entry parameter is not an array');
    	}
    	return result;
    };

    /***
    * Function that takes an array and randomly reorders its items.
    * param {Array} randomizeThis
    * returns randomized array, {Array}
    ***/
    var randomizeOrder = function( randomizeThis ) {
    	// Empty array that will contain the randomized entry parameter
    	var randomizedArray = [],
    		// Caching original length of the array
    		newArrayLength = randomizeThis.length;
    	// check if we have an array as entry parameter
		if ( randomizeThis.constructor === Array ) {
	    	for ( var i = 0; i < newArrayLength; i++ ) {
	    		// add the newly randomized items to the new array
	    		randomizedArray.push(removeRandomItem(randomizeThis));
	    	}
    	} else {
    	    throw new TypeError('Entry parameter is not an array');
    	}

    	return randomizedArray;
    };

    // Another solution,
    // ARRAY RANDOMIZER
    function raRaRandomize ( gagaArray ) {
    	for ( var j, x, i = gagaArray.length; i ; j = Math.floor(Math.random() * i),   x = gagaArray[--i], gagaArray[i] = gagaArray[j], gagaArray[j] = x );
		return gagaArray;
    }
    console.log(raRaRandomize([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]));
    console.log(randomizeOrder(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k']));
    // But which one is faster between the 2 solutions?
};
task17();