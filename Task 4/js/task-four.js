window.onload = function() {
    'use strict';

    var minionSpeak1 = function( speak, consonant ) {
        var arrayOfSpeak, minionArray = [];

        if ( typeof speak === 'string' && consonant.length === 1 && typeof consonant === 'string') {
            arrayOfSpeak = speak.split('');
            for ( var i = 0; i < arrayOfSpeak.length; i++ ) {
                if ( (['a', 'e', 'o', 'u', 'i', 'A', 'E', 'O', 'U', 'I', ' '].indexOf(arrayOfSpeak[i]) != -1 ) ) {
                    minionArray.push( arrayOfSpeak[i] );
                } else {
                    minionArray.push( arrayOfSpeak[i] + consonant + arrayOfSpeak[i] );
                }
            }
        } else {
        throw new TypeError('Enter a string and a single character!');
        }
        return minionArray.join('');
    };

    console.log(minionSpeak1( 'this is fun', 'o'));

    var minionSpeak2 = function( speak, consonant ) {
        var minionArray = '';

        for(var i = 0; i < speak.length; i++) {
            minionArray += (['a', 'e', 'o', 'u', 'i', 'A', 'E', 'O', 'U', 'I', ' '].indexOf(speak[i]) > -1) ? speak[i] : speak[i] + consonant + speak[i];
        }

        return minionArray;
    };

    console.log(minionSpeak2('World domination', 'e'));
};