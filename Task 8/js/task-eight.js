task8 = function() {
    'use strict';
    // Write a function findLongestWord() that takes an array of words and returns the length of the
    // longest one.

    /***
    * Function that takes an array and returns the length of the longest word
    * param {Array of Strings} arrayOfWords
    * returns the length of the longest word, {String}
    ***/
    var findLongestWord = function( arrayOfWords ) {
        var maxLength = 0;
        // Validate that we have an array as a parameter
        if ( arrayOfWords.constructor === Array ) {
            for ( var i = 0; i < arrayOfWords.length; i++ ) {
                // Make sure that we're only dealing with strings
                if ( typeof arrayOfWords[i] === 'string') {
                    if ( arrayOfWords[i].length >= maxLength ) {
                        maxLength = arrayOfWords[i].length;
                    }
                } else {
                    throw new TypeError('Entry parameter is not a word');
                }
            }
        } else {
            throw new TypeError('Entry parameter is not an array');
        }
        return maxLength;
    };

    console.log(findLongestWord( ['GochoMochoChocho', 'Pesho', '123123123', 'Kekeke', 'lorem'] ));
};
task8();