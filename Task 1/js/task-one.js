window.onload = function() {
    'use strict';

    var max = function( num1, num2 ) {
        var max;
        if ( typeof num1 === 'number' && typeof num2 === 'number' ) {
            if( num1 > num2 ) {
                max = num1;
            } else {
                max = num2;
            }
        } else {
            max = 'ENTER NUMBERS!';
        }

        return max;
    };

    console.log(max(42, 69));
};